#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <ctime>
#include "../include/generateRandomQuestion.h"

enum {
    ShiZhongChuangYe = 1,
    WuShiGanWei = 2,
    TanChengQingXi = 3,
    KaiFangQianXun = 4,
    ZhuiQiuJiZhi = 5,
    DuoYuanJianRong = 6
} ZJF;

static OptionMap QMap[] = 
{ {ShiZhongChuangYe, "下面哪种行为体现了始终创业？"},
  {WuShiGanWei, "下面哪种行为体现了务实敢为？"},
  {TanChengQingXi, "下面哪种行为体现了坦诚清晰？"},
  {KaiFangQianXun, "下面哪种行为体现了开放谦逊？"},
  {ZhuiQiuJiZhi, "下面哪种行为体现了追求极致？"},
  {DuoYuanJianRong, "下面哪种行为体现了多元兼容？"}
};

static OptionMap RAMap[] = 
{ {ShiZhongChuangYe, "小张同学xxx[始终创业的正确选项1]"},
  {ShiZhongChuangYe, "小张同学xxx[始终创业的正确选项2]"},
  {WuShiGanWei, "小王同学xxx[务实敢为的正确选项1]"},
  {WuShiGanWei, "小王同学xxx[务实敢为的正确选项2]"},
  {TanChengQingXi, "小赵同学xxx[坦诚清晰的正确选项1]"},
  {TanChengQingXi, "小赵同学xxx[坦诚清晰的正确选项2]"},
  {KaiFangQianXun, "小吴同学xxx[开放谦逊的正确选项1]"},
  {KaiFangQianXun, "小吴同学xxx[开放谦逊的正确选项2]"},
  {ZhuiQiuJiZhi, "小楼同学xxx[追求极致的正确选项1]"},
  {ZhuiQiuJiZhi, "小楼同学xxx[追求极致的正确选项2]"},
  {DuoYuanJianRong, "小于同学xxx[多元兼容的正确选项1]"},
  {DuoYuanJianRong, "小于同学xxx[多元兼容的正确选项2]"}
};

static std::string WAMap[] = {
   "小甲同学不符合字节范儿[字节范儿的错误行为/选项1]",
   "小乙同学不符合字节范儿[字节范儿的错误行为/选项2]",
   "小丙同学不符合字节范儿[字节范儿的错误行为/选项3]",
   "小丁同学不符合字节范儿[字节范儿的错误行为/选项4]",
   "小戊同学不符合字节范儿[字节范儿的错误行为/选项5]",
   "小己同学不符合字节范儿[字节范儿的错误行为/选项6]",
   "小庚同学不符合字节范儿[字节范儿的错误行为/选项7]",
   "小辛同学不符合字节范儿[字节范儿的错误行为/选项8]",
   "小壬同学不符合字节范儿[字节范儿的错误行为/选项9]",
   "小癸同学不符合字节范儿[字节范儿的错误行为/选项10]",
   "小甲同学不符合字节范儿[字节范儿的错误行为/选项11]",
   "小乙同学不符合字节范儿[字节范儿的错误行为/选项12]",
   "小丙同学不符合字节范儿[字节范儿的错误行为/选项13]",
   "小丁同学不符合字节范儿[字节范儿的错误行为/选项14]",
   "小戊同学不符合字节范儿[字节范儿的错误行为/选项15]",
   "小己同学不符合字节范儿[字节范儿的错误行为/选项16]",
   "小庚同学不符合字节范儿[字节范儿的错误行为/选项17]",
   "小辛同学不符合字节范儿[字节范儿的错误行为/选项18]",
   "小壬同学不符合字节范儿[字节范儿的错误行为/选项19]",
   "小癸同学不符合字节范儿[字节范儿的错误行为/选项20]"
};

const char* getQuestion(int id) {
    return QMap[id].value.c_str();
}

const char* getRightAnswer(int id) {
    return RAMap[id].value.c_str();
}

std::vector<int> getQids(int bytedance) {
    std::vector<int> Qids;
    int i, max = DuoYuanJianRong;
    for(int i=0; i<max; i++) {
        if(QMap[i].key == bytedance) {
            Qids.push_back(i);
        }
    }
    return Qids;
}

std::vector<int> getRAids(int bytedance) {
    std::vector<int> RAids;
    int i, max = 2 * DuoYuanJianRong;
    for(int i=0; i<max; i++) {
        if(RAMap[i].key == bytedance) {
            RAids.push_back(i);
        }
    }
    return RAids;
}

const char* getRandomQuestion(int bytedance) {
    std::vector<int> Qids = getQids(bytedance);
    if(Qids.size()==0) {
        std::cout << "error while reading Qids from QMap" << std::endl;
        std::cout << "current bytedance : " << bytedance << std::endl;
        exit(-1);
    }
    srand(time(0));
    int Qnum = rand() % Qids.size();
    return getQuestion(Qids[Qnum]);
}

const char* getRandomRightAnswer(int bytedance) {
    std::vector<int> RAids = getRAids(bytedance);
    if(RAids.size()==0) {
        std::cout << "error while reading RAids from QMap" << std::endl;
        exit(-1);
    }
    static int rcnt = 0;
    rcnt++;
    srand(time(0));
    int RAnum = (rand() % rcnt) % RAids.size();
    return getRightAnswer(RAids[RAnum]);
}

const char* getSpecifiedWrongAnswer(int seq) {
    return WAMap[seq].c_str();
}

std::vector<int> getRandomNumbers(int N, int range, bool diff) {
    std::vector<int> ans;
    int rand_value = 0;
    srand(time(0));
    rand_value = rand() % range;
    ans.push_back(rand_value);
    while(ans.size() < N) {
        srand(time(0)/(rand_value + 1));
        rand_value = rand()%range;
        if((!diff)||(!count(ans.begin(), ans.end(), rand_value))) {
            ans.push_back(rand_value);
        }
    }
    return ans;
}

// Todo4: design a api to record Combined Sequence of Known Full Questions
void showRandomFullQuestions(int turns) {
    // Todo2: here need more effort to makesure 3 different wrong answers
    // qnums表示输入的N个问题，分别对应的可以随机&重复的字节范儿内容
    // false表示可以重复，true表示不能重复
    std::vector<int> qnums = getRandomNumbers(turns, DuoYuanJianRong, false);
#if 0 // debug
    for(int i=0;i<qnums.size();i++) {
        std::cout << "qnums = " << qnums[i] << std::endl;
    }
#endif
    std::vector<int> posRightAns = getRandomNumbers(turns, 4, false);
    int cnt;
    char optionCode;
    for(int i=0; i<qnums.size(); i++) {
        std::cout << "题目" << i+1 << " :" << getRandomQuestion(qnums[i]+1) << std::endl;
        cnt = 0;
        // Todo5: Change 20 -> WAmap size, current WAMap size = 20,
        std::vector<int> wanums = getRandomNumbers(3, 20*(i+1), true);
        for(int j=0; j<4; j++) {
            optionCode = 'A' + j;
            std::cout << optionCode << ". "; 
            if(j==posRightAns[i]) {
                std::cout << getRandomRightAnswer(qnums[i]+1) << std::endl;
            } else {
                std::cout << getSpecifiedWrongAnswer(wanums[cnt]/(i+1)) << std::endl;
                cnt++;
            }
        }
        std::cout << std::endl;
    }
}

void usage(int argc, char *argv[]) {
    std::cout << "Usage: " << argv[0] << " [- t:n:h]" << std::endl;
    std::cout << "    -t       print several (times of) full random questions" << std::endl;
    std::cout << "    -n       print a new random question" << std::endl;
    std::cout << "    -h       print this menu" << std::endl;
}

int main(int argc, char* argv[]) {
    int c, err = -1;
#if 0
    // Todo1: add RapidJson method to load KV Maps from json configuration files on Init stage
    if((err=loadMap()) < 0) {
        std::cout << "Initial load error!" << std::endl;
        exit(-1);
    }
#endif
    std::cout << "Please type to show a new random full question\n" << std::endl;
    do {
        c = getopt(argc, argv, "t:n:h");
        if (c == EOF)
            break;
        switch(c) {
            case 't':
                showRandomFullQuestions(atoi(optarg));
                break;
            // Todo3 : make this avaliable while programme is running
            case 'n':
                showRandomFullQuestions(1);
                break;
            case '?':
                std::cout << argv[0] << ": invalid option -" << optopt;
            case 'h':
                usage(argc, argv);
                exit(1);
        }
    } while(1);
    return 0;   
}
